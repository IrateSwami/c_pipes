#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFFER_LENGTH 10000

// not really a const, just a global variable
char buffer[BUFFER_LENGTH+1];

int main(int args, char *argv[]){

    int read_pipe = atoi(argv[1]);
    int write_pipe = atoi(argv[2]);

    int error_check = 0;
     
    // get the process id
    int pid = (int)getpid();

    // what we're writing to the buffer
    sprintf(buffer, "hi I'm the child process");

    // write to the pipe, note the value returned is the number of characters that were read
    // by the buffer into the pipe
    error_check = write(write_pipe, buffer, strlen(buffer));

    // check for errors
    if(error_check<0){
        perror("child process write error");
        exit(-1);
    }

    // make sure the length of the buffer was proper
    if(error_check!=strlen(buffer)){
        printf("child process error, buffer was a weird size\n");
        exit(-2);
    }

    return 9;
}