#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_LENGTH 10000

int main(void){

    char buffer[BUFFER_LENGTH+1];

    int write_to_slave[2], read_from_slave[2];

    int error_check = 0;
    char param1[10];
    char param2[10];
    pid_t pid;

    // build the write pipe
    error_check = pipe(write_to_slave);
    if(error_check<0){
        perror("write slave pipe creation error");
        exit(-5);
    }

    // build the read pipe
    error_check = pipe(read_from_slave);
    if(error_check<0){
        perror("read slave pipe creation error");
        exit(-6);
    }

    //create a fork, assign that to process id
    pid = fork();

    // we have now forked, using the pid returned by the fork command to keep track of the 
    // parent and child process
    switch(pid) {
        
        // failure
        case -1:
            perror("failed fork");
            exit(-1);
            break;

        // child process        
        case 0:
            // close the read end of write
            error_check = close(write_to_slave[1]);
            if(error_check<0){
                perror("child write slave pipe close failed");
                exit(-6);
            }

            // close the write end of the read    
            error_check = close(read_from_slave[0]);
            if(error_check<0){
                perror("child read slave pipe close failed");
                exit(-7);
            }

            sprintf(param1, "%d", write_to_slave[0]);
            sprintf(param2, "%d", read_from_slave[1]);

            // execute the child process 
            error_check = execl("./slave", "slave", param1, param2, (char*)NULL); 
            if(error_check < 0){
                perror("execl failure");
                exit(-1);
            }

        // parent process 
        default:        
            // close the read end of write
            error_check = close(write_to_slave[0]);
            if(error_check<0){
                perror("parent write slave pipe close failed");
                exit(-3);
            }

            // close the write end of the read    
            error_check = close(read_from_slave[1]);
            if(error_check<0){
                perror("parent read slave pipe close failed");
                exit(-4);
            }

            // read from the child process through the buffer, note that the value returned
            // here is the number of bytes actually read
            error_check = read(read_from_slave[0], buffer, 1000);
            if(error_check<0){
                perror("read from child process to parent error");
                exit(-8);
            }

            // null terminate at the number of bytes read aka error_check
            buffer[error_check] = '\0';

            // print the contents of the buffer 
            printf("buffer contents: %s\n", buffer);

            break;
    }

    // wait for any child process, because -1, BLOCKING
    int status = 0;
    error_check = waitpid(-1, &status, 0);

    if(error_check < 0){
        perror("waitpid error");
        exit(-2);
    }

    // shift the status
    status = (status>>8)&0xff;
    
    if(status != 9){
        printf("status return was not correct: %d\n", status);
    }

    return 0;
}